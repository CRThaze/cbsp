/* CPBSP -- C Programmer's Basic System Profiler
 *
 * a program to provide basic system information
 * which may be useful to a C Programmer developing
 * on the platform.
 *
 * Author: Diego Carrion (0x783czar)
 * Copyright: 2013
 * License: MIT
 *
 */

#include <stdio.h>;
#include <string.h>;

int main()  {
  
  FILE *uname = popen("uname", "r");
  if (!uname)  {
    return -1;
  }
  char buffer[32];
  char *os_name = fgets(buffer, sizeof(buffer), uname);
  pclose(uname);

  if (strcmp(os_name, "ERROR\n") == 0) {
    os_name = "Windows\n";
  }

  int word_size = (int)sizeof(long) * 8;

  printf("\nCPBSP -- C Programmer's Basic System Profiler\n");
  printf("---------------------------------------------\n\n");
  printf("This sytem is running: %s", os_name);
  printf("with a %d-bit enabled processor.\n\n", word_size);
  printf("These are the sizes of the the standard C Data-Types:\n\n");
  printf("the size of a  - 'char'        is:  %-3d byte(s)\n", (int)sizeof(char));
  printf("the size of a  - 'short'       is:  %-3d byte(s)\n", (int)sizeof(short));
  printf("the size of an - 'int'         is:  %-3d byte(s)\n", (int)sizeof(int));
  printf("the size of a  - 'long'        is:  %-3d byte(s)\n", (int)sizeof(long));
  printf("the size of a  - 'long long'   is:  %-3d byte(s)\n", (int)sizeof(long long));
  printf("the size of a  - 'float'       is:  %-3d byte(s)\n", (int)sizeof(float));
  printf("the size of a  - 'double'      is:  %-3d byte(s)\n", (int)sizeof(double));
  printf("the size of a  - 'long double' is:  %-3d byte(s)\n", (int)sizeof(long double));
  printf("\n");
}

