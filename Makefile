
all: cbsp

cbsp:
	mkdir -p bin
	gcc -o bin/cbsp src/cbsp.c

install:
	cp bin/cbsp /usr/local/bin

uninstall:
	rm -f /usr/local/bin/cbsp

clean:
	rm -rf bin

